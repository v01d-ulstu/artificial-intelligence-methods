import pathlib

import pandas as pd
import prettytable


WORKDIR = pathlib.Path(__file__).parent.resolve()
FILE_NAME = "003_titanic.csv"
COLUMN_NAME = "Age"


ages = pd.read_csv(WORKDIR / FILE_NAME)[COLUMN_NAME]

table = prettytable.PrettyTable()
table.title = COLUMN_NAME
table.field_names = ["Max", "Min", "Average", "Median"]
table.add_row(
    [
        ages.max(numeric_only=True),
        ages.min(numeric_only=True),
        ages.mean(),
        ages.median(),
    ]
)

print(table)
