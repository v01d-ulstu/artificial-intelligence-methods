from pathlib import Path

import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from prettytable import PrettyTable


WORKDIR = Path(__file__).parent.resolve()
FILE_NAME = WORKDIR / "data.csv"


dataframe = pd.read_csv(FILE_NAME)

neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(
    [[row[1][1], row[1][2]] for row in dataframe.iterrows()],
    [row[1][3] for row in dataframe.iterrows()],
)

table = PrettyTable()
table.field_names = ["Продукт", "Класс"]

table.add_rows([row[1][0], neigh.predict([[row[1][1], row[1][2]]])[0]] for row in dataframe.iterrows())

print(table)
