# Лаба 1

## Проект и ТЗ

В папке `Project` лежат ТЗ и проект.

## Резюме

Лежит в этой папке.

## Анализ таск-трекеров

Доступен по
[ссылке](https://timeweb.com/ru/community/articles/luchshie-prilozheniya-dlya-planirovaniya-i-kontrolya-zadach).
Я подумал, что копировать контент с этого сайта какой-то файл не имеет
смысла. В статье все описано.

## Другие проекты:

- [ULSTU Home Reading Helper](https://gitlab.com/v01d-ulstu/home-reading-helper)
- [ULSTU Timetable Parser](https://gitlab.com/v01d-ulstu/timetable-parser)
- [GTA SA Cheat](https://gitlab.com/v01d-gamemods/gta-sa)
- [Python Guidelines (методичка по Питону)](https://gitlab.com/v01d-itcube/python-guidelines)
- [(PyPI) Number Base Converter](https://pypi.org/project/nbc/)
- [(PyPI) TrainerBase](https://pypi.org/project/TrainerBase/)
