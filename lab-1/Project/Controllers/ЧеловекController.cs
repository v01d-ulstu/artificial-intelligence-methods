﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ЧеловекController : Controller
    {
        private Model db = new Model();

        // GET: Человек
        public ActionResult Index()
        {
            var человек = db.Человек.Include(ч => ч.Владелец).Include(ч => ч.Сотрудник);
            return View(человек.ToList());
        }

        // GET: Человек/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Человек человек = db.Человек.Find(id);
            if (человек == null)
            {
                return HttpNotFound();
            }
            return View(человек);
        }

        // GET: Человек/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Владелец, "id", "id");
            ViewBag.id = new SelectList(db.Сотрудник, "id", "Должность");
            return View();
        }

        // POST: Человек/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Фамилия,Имя,Отчество,Телефон,Пол,СП")] Человек человек)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Человек.Add(человек);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Владелец, "id", "id", человек.id);
                ViewBag.id = new SelectList(db.Сотрудник, "id", "Должность", человек.id);
                return View(человек);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Человек/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Человек человек = db.Человек.Find(id);
            if (человек == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Владелец, "id", "id", человек.id);
            ViewBag.id = new SelectList(db.Сотрудник, "id", "Должность", человек.id);
            return View(человек);
        }

        // POST: Человек/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Фамилия,Имя,Отчество,Телефон,Пол,СП")] Человек человек)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(человек).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Владелец, "id", "id", человек.id);
                ViewBag.id = new SelectList(db.Сотрудник, "id", "Должность", человек.id);
                return View(человек);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Человек/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Человек человек = db.Человек.Find(id);
            if (человек == null)
            {
                return HttpNotFound();
            }
            return View(человек);
        }

        // POST: Человек/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Человек человек = db.Человек.Find(id);
                db.Человек.Remove(человек);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
