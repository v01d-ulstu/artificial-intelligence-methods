﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ВладелецController : Controller
    {
        private Model db = new Model();

        // GET: Владелец
        public ActionResult Index()
        {
            var владелец = db.Владелец.Include(в => в.Человек);
            return View(владелец.ToList());
        }

        // GET: Владелец/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Владелец владелец = db.Владелец.Find(id);
            if (владелец == null)
            {
                return HttpNotFound();
            }
            return View(владелец);
        }

        // GET: Владелец/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Человек, "id", "id");
            return View();
        }

        // POST: Владелец/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Количество_продаж")] Владелец владелец)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Владелец.Add(владелец);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Человек, "id", "id", владелец.id);
                return View(владелец);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Владелец/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Владелец владелец = db.Владелец.Find(id);
            if (владелец == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Человек, "id", "id", владелец.id);
            return View(владелец);
        }

        // POST: Владелец/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Количество_продаж")] Владелец владелец)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(владелец).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Человек, "id", "id", владелец.id);
                return View(владелец);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Владелец/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Владелец владелец = db.Владелец.Find(id);
            if (владелец == null)
            {
                return HttpNotFound();
            }
            return View(владелец);
        }

        // POST: Владелец/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Владелец владелец = db.Владелец.Find(id);
                db.Владелец.Remove(владелец);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
