﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ОхранаController : Controller
    {
        private Model db = new Model();

        // GET: Охрана
        public ActionResult Index()
        {
            var охрана = db.Охрана.Include(о => о.Магазин);
            return View(охрана.ToList());
        }

        // GET: Охрана/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Охрана охрана = db.Охрана.Find(id);
            if (охрана == null)
            {
                return HttpNotFound();
            }
            return View(охрана);
        }

        // GET: Охрана/Create
        public ActionResult Create()
        {
            ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id");
            return View();
        }

        // POST: Охрана/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,тип,надежность,Магазин_id")] Охрана охрана)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Охрана.Add(охрана);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id", охрана.Магазин_id);
                return View(охрана);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Охрана/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Охрана охрана = db.Охрана.Find(id);
            if (охрана == null)
            {
                return HttpNotFound();
            }
            ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id", охрана.Магазин_id);
            return View(охрана);
        }

        // POST: Охрана/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,тип,надежность,Магазин_id")] Охрана охрана)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(охрана).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id", охрана.Магазин_id);
                return View(охрана);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Охрана/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Охрана охрана = db.Охрана.Find(id);
            if (охрана == null)
            {
                return HttpNotFound();
            }
            return View(охрана);
        }

        // POST: Охрана/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Охрана охрана = db.Охрана.Find(id);
                db.Охрана.Remove(охрана);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
