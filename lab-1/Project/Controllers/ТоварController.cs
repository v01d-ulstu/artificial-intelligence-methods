﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ТоварController : Controller
    {
        private Model db = new Model();

        // GET: Товар
        public ActionResult Index()
        {
            var товар = db.Товар.Include(т => т.Антиквариат).Include(т => т.Владелец).Include(т => т.Другое).Include(т => т.Одежда).Include(т => т.Электроника);
            return View(товар.ToList());
        }

        [HttpPost]
        public ActionResult IndexFindId(int? product_id)
        {
            var товар = db.Товар.Include(т => т.Антиквариат).Include(т => т.Владелец).Include(т => т.Другое).Include(т => т.Одежда).Include(т => т.Электроника).Where(t => t.id == product_id);
            return View("Index", товар.ToList());
        }

        // GET: Товар/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Товар товар = db.Товар.Find(id);
            if (товар == null)
            {
                return HttpNotFound();
            }
            return View(товар);
        }

        // GET: Товар/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Антиквариат, "id", "Происхождение");
            ViewBag.Владелец_id = new SelectList(db.Владелец, "id", "id");
            ViewBag.id = new SelectList(db.Другое, "id", "СвойТип");
            ViewBag.id = new SelectList(db.Одежда, "id", "Размер");
            ViewBag.id = new SelectList(db.Электроника, "id", "Производитель");
            return View();
        }

        // POST: Товар/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Название,Цена,Описание,Состояние,Владелец_id,ДатаСдачи,ПроцентМагазина,ВыкупаетЛиМагазин")] Товар товар)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Товар.Add(товар);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Антиквариат, "id", "Происхождение", товар.id);
                ViewBag.Владелец_id = new SelectList(db.Владелец, "id", "id", товар.Владелец_id);
                ViewBag.id = new SelectList(db.Другое, "id", "СвойТип", товар.id);
                ViewBag.id = new SelectList(db.Одежда, "id", "Размер", товар.id);
                ViewBag.id = new SelectList(db.Электроника, "id", "Производитель", товар.id);
                return View(товар);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Товар/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Товар товар = db.Товар.Find(id);
            if (товар == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Антиквариат, "id", "Происхождение", товар.id);
            ViewBag.Владелец_id = new SelectList(db.Владелец, "id", "id", товар.Владелец_id);
            ViewBag.id = new SelectList(db.Другое, "id", "СвойТип", товар.id);
            ViewBag.id = new SelectList(db.Одежда, "id", "Размер", товар.id);
            ViewBag.id = new SelectList(db.Электроника, "id", "Производитель", товар.id);
            return View(товар);
        }

        // POST: Товар/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Название,Цена,Описание,Состояние,Владелец_id,ДатаСдачи,ПроцентМагазина,ВыкупаетЛиМагазин")] Товар товар)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(товар).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Антиквариат, "id", "Происхождение", товар.id);
                ViewBag.Владелец_id = new SelectList(db.Владелец, "id", "id", товар.Владелец_id);
                ViewBag.id = new SelectList(db.Другое, "id", "СвойТип", товар.id);
                ViewBag.id = new SelectList(db.Одежда, "id", "Размер", товар.id);
                ViewBag.id = new SelectList(db.Электроника, "id", "Производитель", товар.id);
                return View(товар);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Товар/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Товар товар = db.Товар.Find(id);
            if (товар == null)
            {
                return HttpNotFound();
            }
            return View(товар);
        }

        // POST: Товар/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Товар товар = db.Товар.Find(id);
                db.Товар.Remove(товар);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
