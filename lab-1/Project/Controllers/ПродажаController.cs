﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ПродажаController : Controller
    {
        private Model db = new Model();

        // GET: Продажа
        public ActionResult Index()
        {
            var продажа = db.Продажа.Include(п => п.Сотрудник).Include(п => п.Товар);
            return View(продажа.ToList());
        }

        // GET: Продажа/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Продажа продажа = db.Продажа.Find(id);
            if (продажа == null)
            {
                return HttpNotFound();
            }
            return View(продажа);
        }

        // GET: Продажа/Create
        public ActionResult Create()
        {
            ViewBag.Сотрудник_id = new SelectList(db.Сотрудник, "id", "id");
            ViewBag.Товар_id = new SelectList(db.Товар, "id", "id");
            return View();
        }

        // POST: Продажа/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Товар_id,Сотрудник_id,Дата,Цена")] Продажа продажа)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Продажа.Add(продажа);

                    SqlParameter sid = new SqlParameter("@SID", продажа.id);
                    var new_sale_count = db.Database.SqlQuery<int>("EXEC dbo.inc_product_owner_sales @SID", sid);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.Сотрудник_id = new SelectList(db.Сотрудник, "id", "id", продажа.Сотрудник_id);
                ViewBag.Товар_id = new SelectList(db.Товар, "id", "id", продажа.Товар_id);

                return View(продажа);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Продажа/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Продажа продажа = db.Продажа.Find(id);
            if (продажа == null)
            {
                return HttpNotFound();
            }
            ViewBag.Сотрудник_id = new SelectList(db.Сотрудник, "id", "id", продажа.Сотрудник_id);
            ViewBag.Товар_id = new SelectList(db.Товар, "id", "id", продажа.Товар_id);
            return View(продажа);
        }

        // POST: Продажа/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Товар_id,Сотрудник_id,Дата,Цена")] Продажа продажа)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(продажа).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Сотрудник_id = new SelectList(db.Сотрудник, "id", "id", продажа.Сотрудник_id);
                ViewBag.Товар_id = new SelectList(db.Товар, "id", "id", продажа.Товар_id);
                return View(продажа);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Продажа/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Продажа продажа = db.Продажа.Find(id);
            if (продажа == null)
            {
                return HttpNotFound();
            }
            return View(продажа);
        }

        // POST: Продажа/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Продажа продажа = db.Продажа.Find(id);
                db.Продажа.Remove(продажа);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
