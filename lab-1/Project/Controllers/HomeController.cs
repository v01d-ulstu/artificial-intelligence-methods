﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBK.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.Cookies["logged_as"] == null)
            {
                return Redirect("/Home/Login");
            }

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult LoginAsAdmin()
        {
            HttpCookie cookie = new HttpCookie("logged_as");
            cookie.Value = "Admin";
            Response.Cookies.Add(cookie);

            return Redirect("/");
        }

        public ActionResult LoginAsUser()
        {
            HttpCookie cookie = new HttpCookie("logged_as");
            cookie.Value = "User";
            Response.Cookies.Add(cookie);

            return Redirect("/");
        }

        public ActionResult LoginAsProductOwner()
        {
            HttpCookie cookie = new HttpCookie("logged_as");
            cookie.Value = "ProductOwner";
            Response.Cookies.Add(cookie);

            return Redirect("/");
        }

        public ActionResult Logout()
        {
            Response.Cookies["logged_as"].Expires = DateTime.Now.AddDays(-1d);

            return Redirect("/");
        }
    }
}
