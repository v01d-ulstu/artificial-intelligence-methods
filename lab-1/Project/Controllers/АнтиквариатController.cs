﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class АнтиквариатController : Controller
    {
        private Model db = new Model();

        // GET: Антиквариат
        public ActionResult Index()
        {
            var антиквариат = db.Антиквариат.Include(а => а.Товар);
            return View(антиквариат.ToList());
        }


        [HttpPost]
        public ActionResult IndexFilterPlaceAge(string place, int? age)
        {
            var антиквариат = db.Антиквариат.Include(а => а.Товар).Where(a => a.СколькоЛет == age).Where(a => a.Происхождение == place);
            return View("Index", антиквариат.ToList());
        }

        // GET: Антиквариат/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Антиквариат антиквариат = db.Антиквариат.Find(id);
            if (антиквариат == null)
            {
                return HttpNotFound();
            }
            return View(антиквариат);
        }

        // GET: Антиквариат/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Товар, "id", "id");
            return View();
        }

        // POST: Антиквариат/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,СколькоЛет,Происхождение")] Антиквариат антиквариат)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Антиквариат.Add(антиквариат);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Товар, "id", "id", антиквариат.id);
                return View(антиквариат);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Антиквариат/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Антиквариат антиквариат = db.Антиквариат.Find(id);
            if (антиквариат == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Товар, "id", "id", антиквариат.id);
            return View(антиквариат);
        }

        // POST: Антиквариат/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,СколькоЛет,Происхождение")] Антиквариат антиквариат)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(антиквариат).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Товар, "id", "id", антиквариат.id);
                return View(антиквариат);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Антиквариат/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Антиквариат антиквариат = db.Антиквариат.Find(id);
            if (антиквариат == null)
            {
                return HttpNotFound();
            }
            return View(антиквариат);
        }

        // POST: Антиквариат/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Антиквариат антиквариат = db.Антиквариат.Find(id);
                db.Антиквариат.Remove(антиквариат);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
