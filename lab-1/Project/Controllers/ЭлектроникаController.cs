﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ЭлектроникаController : Controller
    {
        private Model db = new Model();

        // GET: Электроника
        public ActionResult Index()
        {
            var электроника = db.Электроника.Include(э => э.Товар);
            return View(электроника.ToList());
        }

        [HttpPost]
        public ActionResult IndexFilterManufacVolt(int? Вольтаж, string Производитель)
        {
            var электроника = db.Электроника.Include(э => э.Товар).Where(e => e.Вольтаж == Вольтаж).Where(e => e.Производитель == Производитель);
            return View("Index", электроника.ToList());
        }

        // GET: Электроника/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Электроника электроника = db.Электроника.Find(id);
            if (электроника == null)
            {
                return HttpNotFound();
            }
            return View(электроника);
        }

        // GET: Электроника/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Товар, "id", "Название");
            return View();
        }

        // POST: Электроника/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Вольтаж,Производитель,Модель")] Электроника электроника)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Электроника.Add(электроника);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Товар, "id", "Название", электроника.id);
                return View(электроника);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Электроника/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Электроника электроника = db.Электроника.Find(id);
            if (электроника == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Товар, "id", "Название", электроника.id);
            return View(электроника);
        }

        // POST: Электроника/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Вольтаж,Производитель,Модель")] Электроника электроника)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(электроника).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Товар, "id", "Название", электроника.id);
                return View(электроника);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Электроника/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Электроника электроника = db.Электроника.Find(id);
            if (электроника == null)
            {
                return HttpNotFound();
            }
            return View(электроника);
        }

        // POST: Электроника/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Электроника электроника = db.Электроника.Find(id);
                db.Электроника.Remove(электроника);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
