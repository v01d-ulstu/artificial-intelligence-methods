﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class СотрудникController : Controller
    {
        private Model db = new Model();

        // GET: Сотрудник
        public ActionResult Index()
        {
            var сотрудник = db.Сотрудник.Include(с => с.Магазин).Include(с => с.Человек);
            return View(сотрудник.ToList());
        }

        // GET: Сотрудник/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудник.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // GET: Сотрудник/Create
        public ActionResult Create()
        {
            ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id");
            ViewBag.id = new SelectList(db.Человек, "id", "id");
            return View();
        }

        // POST: Сотрудник/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Зарплата,Магазин_id,ПринятНаРаботу,Должность")] Сотрудник сотрудник)
        {
            try
            {
                if (сотрудник.Должность == null)
                {
                    сотрудник.Должность = "Продавец";
                }

                if (ModelState.IsValid)
                {
                    db.Сотрудник.Add(сотрудник);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id", сотрудник.Магазин_id);
                ViewBag.id = new SelectList(db.Человек, "id", "id", сотрудник.id);
                return View(сотрудник);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Сотрудник/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудник.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id", сотрудник.Магазин_id);
            ViewBag.id = new SelectList(db.Человек, "id", "id", сотрудник.id);
            return View(сотрудник);
        }

        // POST: Сотрудник/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Зарплата,Магазин_id,ПринятНаРаботу,Должность")] Сотрудник сотрудник)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(сотрудник).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Магазин_id = new SelectList(db.Магазин, "id", "id", сотрудник.Магазин_id);
                ViewBag.id = new SelectList(db.Человек, "id", "id", сотрудник.id);
                return View(сотрудник);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Сотрудник/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Сотрудник сотрудник = db.Сотрудник.Find(id);
            if (сотрудник == null)
            {
                return HttpNotFound();
            }
            return View(сотрудник);
        }

        // POST: Сотрудник/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Сотрудник сотрудник = db.Сотрудник.Find(id);
                db.Сотрудник.Remove(сотрудник);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
