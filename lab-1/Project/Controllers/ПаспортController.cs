﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ПаспортController : Controller
    {
        private Model db = new Model();

        // GET: Паспорт
        public ActionResult Index()
        {
            var паспорт = db.Паспорт.Include(п => п.Человек);
            return View(паспорт.ToList());
        }

        // GET: Паспорт/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Паспорт паспорт = db.Паспорт.Find(id);
            if (паспорт == null)
            {
                return HttpNotFound();
            }
            return View(паспорт);
        }

        // GET: Паспорт/Create
        public ActionResult Create()
        {
            ViewBag.Человек_id = new SelectList(db.Человек, "id", "id");
            return View();
        }

        // POST: Паспорт/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Серия,Номер,КемВыдан,Человек_id")] Паспорт паспорт)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Паспорт.Add(паспорт);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.Человек_id = new SelectList(db.Человек, "id", "id", паспорт.Человек_id);
                return View(паспорт);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Паспорт/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Паспорт паспорт = db.Паспорт.Find(id);
            if (паспорт == null)
            {
                return HttpNotFound();
            }
            ViewBag.Человек_id = new SelectList(db.Человек, "id", "id", паспорт.Человек_id);
            return View(паспорт);
        }

        // POST: Паспорт/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Серия,Номер,КемВыдан,Человек_id")] Паспорт паспорт)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(паспорт).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Человек_id = new SelectList(db.Человек, "id", "id", паспорт.Человек_id);
                return View(паспорт);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Паспорт/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Паспорт паспорт = db.Паспорт.Find(id);
            if (паспорт == null)
            {
                return HttpNotFound();
            }
            return View(паспорт);
        }

        // POST: Паспорт/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Паспорт паспорт = db.Паспорт.Find(id);
                db.Паспорт.Remove(паспорт);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
