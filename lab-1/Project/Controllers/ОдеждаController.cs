﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ОдеждаController : Controller
    {
        private Model db = new Model();

        // GET: Одежда
        public ActionResult Index()
        {
            var одежда = db.Одежда.Include(о => о.Товар);
            return View(одежда.ToList());
        }

        // GET: Одежда/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Одежда одежда = db.Одежда.Find(id);
            if (одежда == null)
            {
                return HttpNotFound();
            }
            return View(одежда);
        }

        // GET: Одежда/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Товар, "id", "id");
            return View();
        }

        // POST: Одежда/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Размер,Цвет,Материал")] Одежда одежда)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Одежда.Add(одежда);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Товар, "id", "id", одежда.id);
                return View(одежда);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Одежда/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Одежда одежда = db.Одежда.Find(id);
            if (одежда == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Товар, "id", "id", одежда.id);
            return View(одежда);
        }

        // POST: Одежда/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Размер,Цвет,Материал")] Одежда одежда)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(одежда).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Товар, "id", "id", одежда.id);
                return View(одежда);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Одежда/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Одежда одежда = db.Одежда.Find(id);
            if (одежда == null)
            {
                return HttpNotFound();
            }
            return View(одежда);
        }

        // POST: Одежда/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Одежда одежда = db.Одежда.Find(id);
                db.Одежда.Remove(одежда);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
