﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class МагазинController : Controller
    {
        private Model db = new Model();

        // GET: Магазин
        public ActionResult Index()
        {
            return View(db.Магазин.ToList());
        }

        // GET: Магазин/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Магазин магазин = db.Магазин.Find(id);
            if (магазин == null)
            {
                return HttpNotFound();
            }
            return View(магазин);
        }

        // GET: Магазин/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Магазин/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Адрес,Рейтинг,Плата_за_аренду,Название,УставнойКапитал")] Магазин магазин)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Магазин.Add(магазин);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(магазин);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // GET: Магазин/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Магазин магазин = db.Магазин.Find(id);
            if (магазин == null)
            {
                return HttpNotFound();
            }
            return View(магазин);
        }

        // POST: Магазин/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Адрес,Рейтинг,Плата_за_аренду,Название,УставнойКапитал")] Магазин магазин)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(магазин).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(магазин);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Магазин/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Магазин магазин = db.Магазин.Find(id);
            if (магазин == null)
            {
                return HttpNotFound();
            }
            return View(магазин);
        }

        // POST: Магазин/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Магазин магазин = db.Магазин.Find(id);
                db.Магазин.Remove(магазин);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
