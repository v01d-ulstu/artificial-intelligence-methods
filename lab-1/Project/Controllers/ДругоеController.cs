﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBK.Models;

namespace DBK.Controllers
{
    public class ДругоеController : Controller
    {
        private Model db = new Model();

        // GET: Другое
        public ActionResult Index()
        {
            var другое = db.Другое.Include(д => д.Товар);
            return View(другое.ToList());
        }

        // GET: Другое/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Другое другое = db.Другое.Find(id);
            if (другое == null)
            {
                return HttpNotFound();
            }
            return View(другое);
        }

        // GET: Другое/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Товар, "id", "id");
            return View();
        }

        // POST: Другое/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,СвойТип")] Другое другое)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Другое.Add(другое);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id = new SelectList(db.Товар, "id", "id", другое.id);
                return View(другое);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Другое/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Другое другое = db.Другое.Find(id);
            if (другое == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Товар, "id", "id", другое.id);
            return View(другое);
        }

        // POST: Другое/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,СвойТип")] Другое другое)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(другое).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id = new SelectList(db.Товар, "id", "id", другое.id);
                return View(другое);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        // GET: Другое/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Другое другое = db.Другое.Find(id);
            if (другое == null)
            {
                return HttpNotFound();
            }
            return View(другое);
        }

        // POST: Другое/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Другое другое = db.Другое.Find(id);
                db.Другое.Remove(другое);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
