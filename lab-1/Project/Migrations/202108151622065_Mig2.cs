namespace DBK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mig2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Сотрудник", "Логин");
            DropColumn("dbo.Сотрудник", "Пароль");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Сотрудник", "Пароль", c => c.String(maxLength: 50));
            AddColumn("dbo.Сотрудник", "Логин", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
