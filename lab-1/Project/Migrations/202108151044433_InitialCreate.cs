namespace DBK.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Антиквариат",
                c => new
                    {
                        id = c.Int(nullable: false),
                        СколькоЛет = c.Int(nullable: false),
                        Происхождение = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Товар", t => t.id, cascadeDelete: true)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.Товар",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Название = c.String(nullable: false, maxLength: 50),
                        Цена = c.Decimal(nullable: false, storeType: "money"),
                        Описание = c.String(maxLength: 300),
                        Состояние = c.Double(nullable: false),
                        Владелец_id = c.Int(nullable: false),
                        ДатаСдачи = c.DateTime(),
                        ПроцентМагазина = c.Int(),
                        ВыкупаетЛиМагазин = c.Boolean(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Владелец", t => t.Владелец_id)
                .Index(t => t.Владелец_id);
            
            CreateTable(
                "dbo.Владелец",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Количествопродаж = c.Int(name: "Количество продаж", nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Человек", t => t.id, cascadeDelete: true)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.Человек",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Фамилия = c.String(nullable: false, maxLength: 50),
                        Имя = c.String(nullable: false, maxLength: 50),
                        Отчество = c.String(maxLength: 50),
                        Телефон = c.String(maxLength: 50),
                        Пол = c.Boolean(nullable: false),
                        СП = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Паспорт",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Серия = c.Int(nullable: false),
                        Номер = c.Int(nullable: false),
                        КемВыдан = c.String(nullable: false, maxLength: 300),
                        Человек_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Человек", t => t.Человек_id)
                .Index(t => t.Человек_id);
            
            CreateTable(
                "dbo.Сотрудник",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Зарплата = c.Decimal(storeType: "money"),
                        Магазин_id = c.Int(nullable: false),
                        ПринятНаРаботу = c.DateTime(nullable: false),
                        Должность = c.String(maxLength: 50),
                        Логин = c.String(nullable: false, maxLength: 50),
                        Пароль = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Магазин", t => t.Магазин_id)
                .ForeignKey("dbo.Человек", t => t.id, cascadeDelete: true)
                .Index(t => t.id)
                .Index(t => t.Магазин_id);
            
            CreateTable(
                "dbo.Магазин",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Адрес = c.String(nullable: false, maxLength: 50),
                        Рейтинг = c.Int(),
                        Платазааренду = c.Decimal(name: "Плата за аренду", storeType: "money"),
                        Название = c.String(maxLength: 50),
                        УставнойКапитал = c.Decimal(storeType: "money"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Охрана",
                c => new
                    {
                        id = c.Int(nullable: false),
                        тип = c.String(maxLength: 50),
                        надежность = c.Double(),
                        Магазин_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Магазин", t => t.Магазин_id)
                .Index(t => t.Магазин_id);
            
            CreateTable(
                "dbo.Продажа",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Товар_id = c.Int(nullable: false),
                        Сотрудник_id = c.Int(nullable: false),
                        Дата = c.DateTime(),
                        Цена = c.Decimal(storeType: "money"),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Сотрудник", t => t.Сотрудник_id)
                .ForeignKey("dbo.Товар", t => t.Товар_id)
                .Index(t => t.Товар_id)
                .Index(t => t.Сотрудник_id);
            
            CreateTable(
                "dbo.Другое",
                c => new
                    {
                        id = c.Int(nullable: false),
                        СвойТип = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Товар", t => t.id, cascadeDelete: true)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.Одежда",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Размер = c.String(nullable: false, maxLength: 10),
                        Цвет = c.String(nullable: false, maxLength: 50),
                        Материал = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Товар", t => t.id, cascadeDelete: true)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.Электроника",
                c => new
                    {
                        id = c.Int(nullable: false),
                        Вольтаж = c.Int(nullable: false),
                        Производитель = c.String(nullable: false, maxLength: 50),
                        Модель = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Товар", t => t.id, cascadeDelete: true)
                .Index(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Электроника", "id", "dbo.Товар");
            DropForeignKey("dbo.Продажа", "Товар_id", "dbo.Товар");
            DropForeignKey("dbo.Одежда", "id", "dbo.Товар");
            DropForeignKey("dbo.Другое", "id", "dbo.Товар");
            DropForeignKey("dbo.Сотрудник", "id", "dbo.Человек");
            DropForeignKey("dbo.Продажа", "Сотрудник_id", "dbo.Сотрудник");
            DropForeignKey("dbo.Сотрудник", "Магазин_id", "dbo.Магазин");
            DropForeignKey("dbo.Охрана", "Магазин_id", "dbo.Магазин");
            DropForeignKey("dbo.Паспорт", "Человек_id", "dbo.Человек");
            DropForeignKey("dbo.Владелец", "id", "dbo.Человек");
            DropForeignKey("dbo.Товар", "Владелец_id", "dbo.Владелец");
            DropForeignKey("dbo.Антиквариат", "id", "dbo.Товар");
            DropIndex("dbo.Электроника", new[] { "id" });
            DropIndex("dbo.Одежда", new[] { "id" });
            DropIndex("dbo.Другое", new[] { "id" });
            DropIndex("dbo.Продажа", new[] { "Сотрудник_id" });
            DropIndex("dbo.Продажа", new[] { "Товар_id" });
            DropIndex("dbo.Охрана", new[] { "Магазин_id" });
            DropIndex("dbo.Сотрудник", new[] { "Магазин_id" });
            DropIndex("dbo.Сотрудник", new[] { "id" });
            DropIndex("dbo.Паспорт", new[] { "Человек_id" });
            DropIndex("dbo.Владелец", new[] { "id" });
            DropIndex("dbo.Товар", new[] { "Владелец_id" });
            DropIndex("dbo.Антиквариат", new[] { "id" });
            DropTable("dbo.Электроника");
            DropTable("dbo.Одежда");
            DropTable("dbo.Другое");
            DropTable("dbo.Продажа");
            DropTable("dbo.Охрана");
            DropTable("dbo.Магазин");
            DropTable("dbo.Сотрудник");
            DropTable("dbo.Паспорт");
            DropTable("dbo.Человек");
            DropTable("dbo.Владелец");
            DropTable("dbo.Товар");
            DropTable("dbo.Антиквариат");
        }
    }
}
