﻿CREATE TABLE [Товар] (
    id                            INTEGER NOT NULL,
    [Название]                    NVARCHAR(50) NOT NULL,
    [Цена]                        MONEY NOT NULL,
    [Описание]                    NVARCHAR(300),
    [Состояние]                   FLOAT NOT NULL,
    [Владелец_id]                 INTEGER NOT NULL,
    [ДатаСдачи]                   DATETIME,
    [ПроцентМагазина]             INTEGER,
    [ВыкупаетЛиМагазин]           BIT,
    CONSTRAINT [PK_Товар] PRIMARY KEY (id)
);

CREATE TABLE [Владелец] (
    id                            INTEGER NOT NULL,
    [Количество продаж]           INTEGER NOT NULL,
    CONSTRAINT [PK_Владелец] PRIMARY KEY (id)
);

CREATE TABLE [Человек] (
    id                            INTEGER NOT NULL,
    [Фамилия]                     NVARCHAR(50) NOT NULL,
    [Имя]                         NVARCHAR(50) NOT NULL,
    [Отчество]                    NVARCHAR(50),
    [Телефон]                     NVARCHAR(50),
    [Пол]                         BIT NOT NULL,
    [СП]                          NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Человек] PRIMARY KEY (id)
);

CREATE TABLE [Сотрудник] (
    [Зарплата]                    MONEY,
    id                            INTEGER NOT NULL,
    [Магазин_id]                  INTEGER NOT NULL,
    [ПринятНаРаботу]              DATETIME NOT NULL,
    [Должность]                   NVARCHAR(50) DEFAULT N'Продавец',
    [Логин]                       NVARCHAR(50) NOT NULL,
    [Пароль]                      NVARCHAR(50),
    CONSTRAINT [PK_Сотрудник] PRIMARY KEY (id)
);

CREATE TABLE [Магазин] (
    id                            INTEGER NOT NULL,
    [Адрес]                       NVARCHAR(50) NOT NULL,
    [Рейтинг]                     INTEGER,
    [Плата за аренду]             MONEY,
    [Название]                    NVARCHAR(50),
    [УставнойКапитал]             MONEY,
    CONSTRAINT [PK_Магазин] PRIMARY KEY (id)
);

CREATE TABLE [Продажа] (
    id                            INTEGER NOT NULL,
    [Товар_id]                    INTEGER NOT NULL,
    [Сотрудник_id]                INTEGER NOT NULL,
    [Дата]                        DATETIME,
    [Цена]                        MONEY,
    CONSTRAINT [PK_Продажа] PRIMARY KEY (id)
);

CREATE TABLE [Охрана] (
    id                            INTEGER NOT NULL,
    [тип]                         NVARCHAR(50),
    [надежность]                  FLOAT,
    [Магазин_id]                  INTEGER NOT NULL,
    CONSTRAINT [PK_Охрана] PRIMARY KEY (id)
);

CREATE TABLE [Паспорт] (
    id                            INTEGER NOT NULL,
    [Серия]                       INTEGER NOT NULL,
    [Номер]                       INTEGER NOT NULL,
    [КемВыдан]                    NVARCHAR(300) NOT NULL,
    [Человек_id]                  INTEGER NOT NULL,
    CONSTRAINT [PK_Паспорт] PRIMARY KEY (id)
);

CREATE TABLE [Другое] (
    id                            INTEGER NOT NULL,
    [СвойТип]                     NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_Другое] PRIMARY KEY (id)
);

CREATE TABLE [Электроника] (
    id                            INTEGER NOT NULL,
    [Вольтаж]                     INTEGER NOT NULL,
    [Производитель]               NVARCHAR(50) NOT NULL,
    [Модель]                      NVARCHAR(50),
    CONSTRAINT [PK_Электроника] PRIMARY KEY (id)
);

CREATE TABLE [Одежда] (
    [Размер]                      NVARCHAR(10)NOT NULL,
    [Цвет]                        NVARCHAR(50) NOT NULL,
    [Материал]                    NVARCHAR(50),
    id                            INTEGER NOT NULL,
    CONSTRAINT [PK_Одежда] PRIMARY KEY (id)
);

CREATE TABLE [Антиквариат] (
    [СколькоЛет]                  INTEGER NOT NULL,
    [Происхождение]               NVARCHAR(50),
    id                            INTEGER NOT NULL,
    CONSTRAINT [PK_Антиквариат] PRIMARY KEY (id)
);

ALTER TABLE [Сотрудник]
    ADD CONSTRAINT [Сотрудник_Человек_FK_Rule]
    FOREIGN KEY (id)
    REFERENCES [Человек]
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE [Владелец]
    ADD CONSTRAINT [Владелец_Человек_FK_Rule]
    FOREIGN KEY (id)
    REFERENCES [Человек]
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE [Сотрудник]
    ADD CONSTRAINT [Сотрудник_Магазин_FK_Rule]
    FOREIGN KEY ([Магазин_id])
    REFERENCES [Магазин];
ALTER TABLE [Товар]
    ADD CONSTRAINT [Товар _Владелец_FK_Rule]
    FOREIGN KEY ([Владелец_id])
    REFERENCES [Владелец];
ALTER TABLE [Продажа]
    ADD CONSTRAINT [Продажа_Товар _FK_Rule]
    FOREIGN KEY ([Товар_id])
    REFERENCES [Товар];
ALTER TABLE [Продажа]
    ADD CONSTRAINT [Продажа_Сотрудник_FK_Rule]
    FOREIGN KEY ([Сотрудник_id])
    REFERENCES [Сотрудник];
ALTER TABLE [Охрана]
    ADD CONSTRAINT [Охрана_Магазин_FK_Rule]
    FOREIGN KEY ([Магазин_id])
    REFERENCES [Магазин];
ALTER TABLE [Другое]
    ADD CONSTRAINT [Другое_Товар _FK_Rule]
    FOREIGN KEY (id)
    REFERENCES [Товар]
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE [Электроника]
    ADD CONSTRAINT [Электроника_Товар _FK_Rule]
    FOREIGN KEY (id)
    REFERENCES [Товар]
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE [Одежда]
    ADD CONSTRAINT [Одежда_Товар _FK_Rule]
    FOREIGN KEY (id)
    REFERENCES [Товар]
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE [Антиквариат]
    ADD CONSTRAINT [Антиквариат_Товар _FK_Rule]
    FOREIGN KEY (id)
    REFERENCES [Товар]
    ON UPDATE CASCADE
    ON DELETE CASCADE;
ALTER TABLE [Паспорт]
    ADD CONSTRAINT [Паспорт_Человек_FK_Rule]
    FOREIGN KEY ([Человек_id])
    REFERENCES [Человек];
