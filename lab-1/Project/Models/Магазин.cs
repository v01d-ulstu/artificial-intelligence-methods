namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Магазин
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Магазин()
        {
            Охрана = new HashSet<Охрана>();
            Сотрудник = new HashSet<Сотрудник>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string Адрес { get; set; }

        public int? Рейтинг { get; set; }

        [Column("Плата за аренду", TypeName = "money")]
        public decimal? Плата_за_аренду { get; set; }

        [StringLength(50)]
        public string Название { get; set; }

        [Column(TypeName = "money")]
        public decimal? УставнойКапитал { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Охрана> Охрана { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Сотрудник> Сотрудник { get; set; }
    }
}
