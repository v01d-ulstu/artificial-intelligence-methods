namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Одежда
    {
        [Required]
        [StringLength(10)]
        public string Размер { get; set; }

        [Required]
        [StringLength(50)]
        public string Цвет { get; set; }

        [StringLength(50)]
        public string Материал { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public virtual Товар Товар { get; set; }
    }
}
