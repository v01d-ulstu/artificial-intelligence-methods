namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Охрана
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(50)]
        public string тип { get; set; }

        public double? надежность { get; set; }

        public int Магазин_id { get; set; }

        public virtual Магазин Магазин { get; set; }
    }
}
