namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Сотрудник
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Сотрудник()
        {
            Продажа = new HashSet<Продажа>();
        }

        [Column(TypeName = "money")]
        public decimal? Зарплата { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int Магазин_id { get; set; }

        public DateTime ПринятНаРаботу { get; set; }

        [StringLength(50)]
        public string Должность { get; set; }

        public virtual Магазин Магазин { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Продажа> Продажа { get; set; }

        public virtual Человек Человек { get; set; }
    }
}
