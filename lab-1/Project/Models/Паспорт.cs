namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Паспорт
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int Серия { get; set; }

        public int Номер { get; set; }

        [Required]
        [StringLength(300)]
        public string КемВыдан { get; set; }

        public int Человек_id { get; set; }

        public virtual Человек Человек { get; set; }
    }
}
