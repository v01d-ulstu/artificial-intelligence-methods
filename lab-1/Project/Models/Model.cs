﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace DBK.Models
{
    public partial class Model : DbContext
    {
        public Model()
            : base("name=DBK")
        {
        }

        public virtual DbSet<Антиквариат> Антиквариат { get; set; }
        public virtual DbSet<Владелец> Владелец { get; set; }
        public virtual DbSet<Другое> Другое { get; set; }
        public virtual DbSet<Магазин> Магазин { get; set; }
        public virtual DbSet<Одежда> Одежда { get; set; }
        public virtual DbSet<Охрана> Охрана { get; set; }
        public virtual DbSet<Паспорт> Паспорт { get; set; }
        public virtual DbSet<Продажа> Продажа { get; set; }
        public virtual DbSet<Сотрудник> Сотрудник { get; set; }
        public virtual DbSet<Товар> Товар { get; set; }
        public virtual DbSet<Человек> Человек { get; set; }
        public virtual DbSet<Электроника> Электроника { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Владелец>()
                .HasMany(e => e.Товар)
                .WithRequired(e => e.Владелец)
                .HasForeignKey(e => e.Владелец_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Магазин>()
                .Property(e => e.Плата_за_аренду)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Магазин>()
                .Property(e => e.УставнойКапитал)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Магазин>()
                .HasMany(e => e.Охрана)
                .WithRequired(e => e.Магазин)
                .HasForeignKey(e => e.Магазин_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Магазин>()
                .HasMany(e => e.Сотрудник)
                .WithRequired(e => e.Магазин)
                .HasForeignKey(e => e.Магазин_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Продажа>()
                .Property(e => e.Цена)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Сотрудник>()
                .Property(e => e.Зарплата)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Сотрудник>()
                .HasMany(e => e.Продажа)
                .WithRequired(e => e.Сотрудник)
                .HasForeignKey(e => e.Сотрудник_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Товар>()
                .Property(e => e.Цена)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Товар>()
                .HasOptional(e => e.Антиквариат)
                .WithRequired(e => e.Товар)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Товар>()
                .HasOptional(e => e.Другое)
                .WithRequired(e => e.Товар)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Товар>()
                .HasOptional(e => e.Одежда)
                .WithRequired(e => e.Товар)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Товар>()
                .HasMany(e => e.Продажа)
                .WithRequired(e => e.Товар)
                .HasForeignKey(e => e.Товар_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Товар>()
                .HasOptional(e => e.Электроника)
                .WithRequired(e => e.Товар)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Человек>()
                .HasOptional(e => e.Владелец)
                .WithRequired(e => e.Человек)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Человек>()
                .HasMany(e => e.Паспорт)
                .WithRequired(e => e.Человек)
                .HasForeignKey(e => e.Человек_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Человек>()
                .HasOptional(e => e.Сотрудник)
                .WithRequired(e => e.Человек)
                .WillCascadeOnDelete();
        }
    }
}
