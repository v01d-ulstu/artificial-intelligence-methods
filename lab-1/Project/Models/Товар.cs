namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Товар
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Товар()
        {
            Продажа = new HashSet<Продажа>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string Название { get; set; }

        [Column(TypeName = "money")]
        public decimal Цена { get; set; }

        [StringLength(300)]
        public string Описание { get; set; }

        public double Состояние { get; set; }

        [Display(Name = "Владелец ID")]
        public int Владелец_id { get; set; }

        public DateTime? ДатаСдачи { get; set; }

        public int? ПроцентМагазина { get; set; }

        public bool? ВыкупаетЛиМагазин { get; set; }

        public virtual Антиквариат Антиквариат { get; set; }

        public virtual Владелец Владелец { get; set; }

        public virtual Другое Другое { get; set; }

        public virtual Одежда Одежда { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Продажа> Продажа { get; set; }

        public virtual Электроника Электроника { get; set; }
    }
}
