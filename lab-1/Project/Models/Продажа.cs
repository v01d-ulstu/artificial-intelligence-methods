namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Продажа
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int Товар_id { get; set; }

        public int Сотрудник_id { get; set; }

        public DateTime? Дата { get; set; }

        [Column(TypeName = "money")]
        public decimal? Цена { get; set; }

        public virtual Сотрудник Сотрудник { get; set; }

        public virtual Товар Товар { get; set; }
    }
}
