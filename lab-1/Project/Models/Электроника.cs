namespace DBK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Электроника
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int Вольтаж { get; set; }

        [Required]
        [StringLength(50)]
        public string Производитель { get; set; }

        [StringLength(50)]
        public string Модель { get; set; }

        public virtual Товар Товар { get; set; }
    }
}
