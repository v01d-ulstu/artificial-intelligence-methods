import matplotlib.pyplot as pyplot
import pandas
import pylab
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC


df = pandas.read_csv("alcohol-consumption.csv")
teach_df = df.iloc[:95]
test_df = df.iloc[95:]


def init(df):
    df = df.copy()
    df = df.dropna()
    df = df.drop("2020_projection", axis=1)
    df = df.drop("2025_projection", axis=1)
    X = df.drop("continent", axis=1)
    y = df["continent"]
    X = pandas.DataFrame(X, index=X.index, columns=X.columns)
    return X, y


scaler = StandardScaler()
X_teach, y_teach = init(teach_df)
X_teach = scaler.fit_transform(X_teach)
X_test, y_test = init(test_df)
X_test = scaler.fit_transform(X_test)

knn = KNeighborsClassifier().fit(X_teach, y_teach)
knn_predictions = pandas.Series(knn.predict(X_test))
print("Точность предсказаний методом KNN: " + str(knn.score(X_test, y_test) * 100) + "%")

lda = LinearDiscriminantAnalysis().fit(X_teach, y_teach)
lda_predictions = pandas.Series(lda.predict(X_test))
print("Точность предсказаний методом LDA: " + str(lda.score(X_test, y_test) * 100) + "%")

svm = SVC(kernel="rbf").fit(X_teach, y_teach)
svm_predictions = pandas.Series(svm.predict(X_test))
print("Точность предсказаний методом SVM: " + str(svm.score(X_test, y_test) * 100) + "%")

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%")
pyplot.title("Реальный континент")
pylab.subplot(1, 2, 2)
pyplot.pie(knn_predictions.value_counts().sort_index(), labels=sorted(knn_predictions.unique()), autopct="%1.1f%%")
pyplot.title("Континент по KNN")
pyplot.show()

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%")
pyplot.title("Реальный континент")
pylab.subplot(1, 2, 2)
pyplot.pie(lda_predictions.value_counts().sort_index(), labels=sorted(lda_predictions.unique()), autopct="%1.1f%%")
pyplot.title("Континент по LDA")
pyplot.show()

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%")
pyplot.title("Реальный континент")
pylab.subplot(1, 2, 2)
pyplot.pie(svm_predictions.value_counts().sort_index(), labels=sorted(svm_predictions.unique()), autopct="%1.1f%%")
pyplot.title("Континент по SVN")
pyplot.show()
